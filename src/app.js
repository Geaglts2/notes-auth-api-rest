const express = require("express");
const morgan = require("morgan");
const cors = require("cors");

const app = express();

//config server
app.use(cors());
app.use(morgan("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

//routes
const { noteRoutes } = require("./routes");

app.set("PORT", process.env.PORT || 3000);

app.get("/api/init", (req, res) => {
    res.json({
        message: "HELLO WORLD!!",
    });
});

app.use("/api/notes", noteRoutes);

app.use("*", (req, res) => {
    res.json({ message: "not found" }).status(404);
});

module.exports = app;
