//models
const Note = require("../models/notes.model");

//validations
const { inputValidations } = require("../validations");

const notesController = {};

notesController.getAll = async (req, res) => {
    try {
        const notes = await Note.find();

        res.json({
            data: notes,
            loading: false,
            message: "Good",
        });
    } catch (err) {
        res.json({
            message: "🐻",
            error: err,
        });
    }
};

notesController.getOne = async (req, res) => {
    try {
        const { id } = req.params;

        const note = await Note.findById(id);

        res.json({
            note,
            loading: false,
            message: "Good",
        });
    } catch (err) {
        res.json({
            message: "🐻",
        });
    }
};

notesController.createNew = async (req, res) => {
    try {
        const { name, description } = req.body;

        const validName = inputValidations.validInput(name, 4, "name");

        if (validName.error) {
            return res.json({
                message: validName.mensaje,
            });
        }

        const newNote = new Note({
            name,
            description,
        });

        const note = await Note.create(newNote);

        res.json({ note, loading: false });
    } catch (err) {
        console.log(err);

        res.json({
            message: "This Isn't My Error >:(",
        });
    }
};

notesController.updateExist = async (req, res) => {
    try {
        const { id } = req.params;

        await Note.findByIdAndUpdate(id, req.body);

        res.json({ message: "Note updated", loading: false, status: "good" });
    } catch (err) {
        res.json({
            message: "Este no es un id valido",
            status: "bad",
        });
    }
};

notesController.deleteExist = async (req, res) => {
    try {
        const { id } = req.params;

        await Note.findByIdAndDelete(id);
        res.json({ message: "Note deleted", loading: false, status: "good" });
    } catch (err) {
        res.json({
            message: "Este no es un id valido",
            status: "bad",
        });
    }
};

module.exports = notesController;
