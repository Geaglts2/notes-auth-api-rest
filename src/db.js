const mongoose = require("mongoose");

const MONGO_URI = process.env.MONGODB_URI;

mongoose
    .connect(MONGO_URI, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false,
    })
    .then(() => {
        console.log("db status: :)");
    })
    .catch(() => {
        console.log("db status: :(");
    });
