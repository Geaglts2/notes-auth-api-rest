require("dotenv").config();

const app = require("./app");
require("./db");

function main() {
    app.listen(app.get("PORT"), () => {
        console.log(
            "Server listening on http://localhost:" +
                app.get("PORT") +
                "/api/init"
        );
    });
}

main();
