const { Schema, model } = require("mongoose");

const noteSchema = new Schema(
    {
        name: { type: String, required: true },
        description: String,
    },
    { timestamps: true }
);

module.exports = model("Note", noteSchema, "notes");
