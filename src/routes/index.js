const noteRoutes = require("./notes.routes");

module.exports = {
    noteRoutes,
};
