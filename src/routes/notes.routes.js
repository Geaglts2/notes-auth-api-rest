const { Router } = require("express");

const router = Router();

const noteController = require("../controllers/notes.controllers");

router.get("/all", noteController.getAll);

router.post("/new", noteController.createNew);

router
    .route("/note/:id")
    .get(noteController.getOne)
    .put(noteController.updateExist)
    .delete(noteController.deleteExist);

module.exports = router;
