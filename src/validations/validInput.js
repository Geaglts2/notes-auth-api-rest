const validInput = (field, max, name, required) => {
    let error = false,
        mensaje = "";

    if (field === undefined) {
        error = true;
        mensaje = `El campo ${name} es requerido`;

        return { error, mensaje };
    } else {
        if (field.length < max) {
            if (max === 1) {
                error = true;
                mensaje = `El campo ${name} debe tener como minimo ${max} carecter`;
                return { error, mensaje };
            }
            error = true;
            mensaje = `El campo ${name} debe tener como minimo ${max} caracteres`;
            return { error, mensaje };
        }
    }

    return { error };
};

module.exports = { validInput };
